package com.dailyActivity.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.dailyActivity.dto.DailyActivityDto;
import com.dailyActivity.dto.DailyActivityResponseDto;
import com.dailyActivity.entity.DailyActivity;
import com.dailyActivity.enumeration.ErrorMessage;
import com.dailyActivity.exception.DailyActivityNotFoundException;
import com.dailyActivity.repository.DailyActivityRepository;
import com.dailyActivity.service.DailyActivityService;

@Service
public class DailyActivityServiceImpl implements DailyActivityService {

	@Autowired
	DailyActivityRepository activityRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DailyActivityServiceImpl.class);

	@Override
	public boolean createDailyActivity(List<DailyActivityDto> dailyActivityDtos) {
		
		// TODO Auto-generated method stub
		dailyActivityDtos.stream().forEach(activityDto -> {
			DailyActivity dailyActivity = new DailyActivity();
			BeanUtils.copyProperties(activityDto, dailyActivity);
			dailyActivity.setActivityDate(LocalDate.now());
			activityRepository.save(dailyActivity);
		});
		
		return true;
	}

	@Override
	public List<DailyActivityResponseDto> getAllActivityDetails(int pageSize, int pageNumber) {
		List<DailyActivity> activities = new ArrayList<>();
		Pageable pageable = PageRequest.of(pageNumber, pageSize);
		activities = activityRepository.findAll(pageable).getContent();
		activities = activityRepository.findAll();

		if (activities.size() == 0)
			throw new DailyActivityNotFoundException(ErrorMessage.DAILYACTIVITYNOTFOUNDEXCEPTION.getMessage());
		return prepareDtos(activities);
	}

	@Override
	public List<DailyActivityResponseDto> getActivityBasedOnCode(Long employeeCode) {
		// TODO Auto-generated method stub
		List<DailyActivity> activities = activityRepository.findByemployeeCode(employeeCode);
		if (activities.size() == 0)
			throw new DailyActivityNotFoundException(ErrorMessage.DAILYACTIVITYNOTFOUNDEXCEPTION.getMessage());
		return prepareDtos(activities);
	}

	@Override
	public boolean modifyActivity(Map<String, Object> updates, Long employeeCode,Long activityId) {
		// TODO Auto-generated method stub
		DailyActivity activity = activityRepository.findByemployeeCodeAndactivityId(employeeCode, activityId);
		if (activity == null)
			throw new DailyActivityNotFoundException(ErrorMessage.DAILYACTIVITYNOTFOUNDEXCEPTION.getMessage());
		for (Map.Entry<String, Object> entry : updates.entrySet()) {
			try {
				org.apache.commons.beanutils.BeanUtils.setProperty(activity, entry.getKey(), entry.getValue());
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		activityRepository.save(activity);
		return true;
	}

	@Override
	public boolean deleteDailyActivity(Long employeeCode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<DailyActivityResponseDto> getAllActivityDetailsBetweenDates(LocalDate startDate, LocalDate endDate) {
		List<DailyActivity> activities = new ArrayList<>();
		activities = activityRepository.findAllByActivityDateBetween(startDate, endDate);
		if (activities.size() == 0)
			throw new DailyActivityNotFoundException(ErrorMessage.DAILYACTIVITYNOTFOUNDEXCEPTION.getMessage());
		return prepareDtos(activities);
	}

	@Override
	public List<DailyActivityResponseDto> getAllActivityDetailsOnDate(LocalDate activityDate) {
		List<DailyActivity> activities = new ArrayList<>();
		activities = activityRepository.findByactivityDate(activityDate);
		if (activities.size() == 0)
			throw new DailyActivityNotFoundException(ErrorMessage.DAILYACTIVITYNOTFOUNDEXCEPTION.getMessage());
		return prepareDtos(activities);
	}

	@Override
	public List<DailyActivityResponseDto> getAllActivityDetailsBetweenDatesOfEmployee(LocalDate startdate,
			LocalDate enddate, Long employeeCode) {
		List<DailyActivity> activities = new ArrayList<>();
		activities = activityRepository.findAllByActivityDateBetweenOfEmployee(startdate, enddate, employeeCode);
		if (activities.size() == 0)
			throw new DailyActivityNotFoundException(ErrorMessage.DAILYACTIVITYNOTFOUNDEXCEPTION.getMessage());
		return prepareDtos(activities);
	}
	
	private List<DailyActivityResponseDto> prepareDtos(List<DailyActivity> activities) {
		List<DailyActivityResponseDto> activityDtos = new ArrayList<>();
		activities.stream().forEach(activity -> {
			DailyActivityResponseDto activityDto = new DailyActivityResponseDto();
			BeanUtils.copyProperties(activity, activityDto);
			activityDtos.add(activityDto);
		});

		return activityDtos;
	}
}
