package com.dailyActivity.service;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import com.dailyActivity.dto.DailyActivityDto;
import com.dailyActivity.dto.DailyActivityResponseDto;

public interface DailyActivityService {

	boolean createDailyActivity(List<DailyActivityDto> dailyActivityDtos);

	List<DailyActivityResponseDto> getAllActivityDetails(int pageSize, int pageNumber);

	List<DailyActivityResponseDto> getActivityBasedOnCode(Long employeeCode);

	boolean deleteDailyActivity(Long employeeCode);

	List<DailyActivityResponseDto> getAllActivityDetailsBetweenDates(LocalDate startDate, LocalDate endDate);

	List<DailyActivityResponseDto> getAllActivityDetailsOnDate(LocalDate activityDate);

	List<DailyActivityResponseDto> getAllActivityDetailsBetweenDatesOfEmployee(LocalDate startdate, LocalDate enddate,
			Long employeeCode);

	boolean modifyActivity(Map<String, Object> updates, Long employeeCode, Long activityId);

}
