package com.dailyActivity.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.dailyActivity.entity.DailyActivity;

@Repository
public interface DailyActivityRepository extends JpaRepository<DailyActivity, Long> {

	public List<DailyActivity> findByemployeeCode(Long employeeCode);
	

	@Query("select d from DailyActivity d where d.employeeCode = :employeeCode and d.activityId = :activityId")
	public DailyActivity findByemployeeCodeAndactivityId(@Param("employeeCode") Long employeeCode,@Param("activityId") Long activityId);

	@Query("select d from DailyActivity d where d.activityDate >= :dateTimeStart and d.activityDate <= :dateTimeEnd")
	List<DailyActivity> findAllByActivityDateBetween(@Param("dateTimeStart") LocalDate dateTimeStart,
			@Param("dateTimeEnd") LocalDate dateTimeEnd);

	@Query("select d from DailyActivity d where d.activityDate = :activityDate")
	List<DailyActivity> findByactivityDate(@Param("activityDate") LocalDate activityDate);

	@Query("select d from DailyActivity d where d.activityDate >= :dateTimeStart and d.activityDate <= :dateTimeEnd and d.employeeCode = :employeeCode")
	List<DailyActivity> findAllByActivityDateBetweenOfEmployee(@Param("dateTimeStart") LocalDate dateTimeStart,
			@Param("dateTimeEnd") LocalDate dateTimeEnd, @Param("employeeCode") Long employeeCode);
}
