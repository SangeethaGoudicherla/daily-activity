package com.dailyActivity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DailyActivityApplication {

	public static void main(String[] args) {
		SpringApplication.run(DailyActivityApplication.class, args);
	}

}
