package com.dailyActivity.exception;

public class DailyActivityNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public DailyActivityNotFoundException(String message) {
		super(message);
	}


}
