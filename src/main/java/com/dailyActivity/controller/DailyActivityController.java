package com.dailyActivity.controller;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dailyActivity.dto.DailyActivityDto;
import com.dailyActivity.dto.DailyActivityResponseDto;
import com.dailyActivity.service.DailyActivityService;

@Validated
@RestController
@RequestMapping("/api/DailyActivity")
public class DailyActivityController {

	@Autowired
	DailyActivityService dailyActivityService;

	@PostMapping("/create")
	public ResponseEntity<String> createDailyActivity(
			@Valid @RequestBody @NotEmpty(message = "Daily activity cannot be empty") List<DailyActivityDto> dailyActivityDtos)
			throws Exception {
		boolean isSaved = dailyActivityService.createDailyActivity(dailyActivityDtos);
		if (isSaved)
			return new ResponseEntity<>("Successfully Added DailyActivity(s)", HttpStatus.CREATED);
		return new ResponseEntity<>("Failed to add DailyActivity(s)", HttpStatus.BAD_REQUEST);
	}

	@GetMapping("")
	public ResponseEntity<List<DailyActivityResponseDto>> getAllActivityDetails(@RequestParam int pageSize,
			@RequestParam int pageNumber) {
		return new ResponseEntity<List<DailyActivityResponseDto>>(
				dailyActivityService.getAllActivityDetails(pageSize, pageNumber), HttpStatus.OK);
	}

	@GetMapping("/datesBetween")
	public ResponseEntity<List<DailyActivityResponseDto>> getAllActivityDetailsBetweenDates(
			@RequestParam("startdate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startdate,
			@RequestParam("enddate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate enddate) {
		return new ResponseEntity<List<DailyActivityResponseDto>>(
				dailyActivityService.getAllActivityDetailsBetweenDates(startdate, enddate), HttpStatus.OK);
	}

	@GetMapping("/date")
	public ResponseEntity<List<DailyActivityResponseDto>> getAllActivityDetailsOnADate(
			@RequestParam("activityDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate activityDate) {
		return new ResponseEntity<List<DailyActivityResponseDto>>(
				dailyActivityService.getAllActivityDetailsOnDate(activityDate), HttpStatus.OK);
	}

	@GetMapping("/{employeeCode}/datesBetween")
	public ResponseEntity<List<DailyActivityResponseDto>> getAllActivityDetailsBetweenDatesOfEmployee(
			@PathVariable Long employeeCode,
			@RequestParam("startdate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate startdate,
			@RequestParam("enddate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate enddate) {
		return new ResponseEntity<List<DailyActivityResponseDto>>(
				dailyActivityService.getAllActivityDetailsBetweenDatesOfEmployee(startdate, enddate, employeeCode),
				HttpStatus.OK);
	}

	@GetMapping("{employeeCode}")
	public ResponseEntity<List<DailyActivityResponseDto>> getActivityByEmployeeCode(@PathVariable Long employeeCode) {
		return new ResponseEntity<List<DailyActivityResponseDto>>(
				dailyActivityService.getActivityBasedOnCode(employeeCode), HttpStatus.OK);
	}

	@PatchMapping("/modify")
	public ResponseEntity<String> modifyDailyActivity(@RequestBody Map<String, Object> updates,
			@RequestParam Long employeeCode, @RequestParam Long activityId) {

		boolean isUpdated = dailyActivityService.modifyActivity(updates, employeeCode, activityId);
		if (isUpdated)
			return new ResponseEntity<>("Successfully modified activity", HttpStatus.OK);

		return new ResponseEntity<>("Failed to modify activity", HttpStatus.BAD_REQUEST);
	}

	@DeleteMapping("{employeeCode}")
	public ResponseEntity<String> deleteDailyActivity(@PathVariable Long employeeCode) {
		boolean isMarkedAsDeleted = dailyActivityService.deleteDailyActivity(employeeCode);
		if (isMarkedAsDeleted)
			return new ResponseEntity<>("Successfully Marked DailyActivity as deleted", HttpStatus.OK);
		return new ResponseEntity<>("Failed to mark delete DailyActivity", HttpStatus.BAD_REQUEST);
	}

}
