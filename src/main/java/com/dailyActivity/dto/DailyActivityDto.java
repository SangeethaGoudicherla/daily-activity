package com.dailyActivity.dto;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotEmpty;

public class DailyActivityDto {
	
	@Digits(integer = 10, fraction = 1)
	private Long employeeCode;

	@NotEmpty(message = "Description should not be empty")
	private String activityDescription;

	@NotEmpty(message = "Activity Status should not be empty")
	private String activityStatus;

	public Long getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(Long employeeCode) {
		this.employeeCode = employeeCode;
	}

	public String getActivityDescription() {
		return activityDescription;
	}

	public void setActivityDescription(String activityDescription) {
		this.activityDescription = activityDescription;
	}

	public String getActivityStatus() {
		return activityStatus;
	}

	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

}
