package com.dailyActivity.enumeration;

public enum ErrorMessage {
	DAILYACTIVITYNOTFOUNDEXCEPTION("DailyActivity is not yet recorded in database");
	
	private String message;

	public String getMessage() {
		return message;
	}

	private ErrorMessage(String message) {
		this.message = message;
	}

}
